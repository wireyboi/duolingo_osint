# Duolingo OSINT

View account details and stats from a Duolingo user from an open endpoint

Usage `./duolingoOSINT <username>`

## Example
```Username dan
Name: Dan James
Avatar: https://simg-ssl.duolingo.com/avatars/69/VkmQ0LYQab/xxlarge
Streak: 0
Motivation: none
Plus: true
Country: Canada
Created At: "2011-12-01 07:34:28 +00:00"
Courses: (1)
        English -> Spanish [DUOLINGO_ES_EN]
                XP: 7809
                Crowns: 71