extern crate reqwest;
#[macro_use]
extern crate serde_derive;
extern crate serde_json;
extern crate chrono;
use std::{time::Duration, process::exit};

use chrono::{Local, TimeZone};

static ENDPOINT_USERNAME: &str = "https://www.duolingo.com/2017-06-30/users?username={{USERNAME}}";
//static ENDPOINT_EMAIL: &str = "https://www.duolingo.com/2017-06-30/users?email={{EMAIL}}";

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Duolingo {
    #[serde(rename = "users")]
    users: Vec<User>,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct User {
    #[serde(rename = "joinedClassroomIds")]
    joined_classroom_ids: Vec<Option<serde_json::Value>>,

    #[serde(rename = "streak")]
    streak: i64,

    #[serde(rename = "motivation")]
    motivation: String,

    #[serde(rename = "acquisitionSurveyReason")]
    acquisition_survey_reason: String,

    #[serde(rename = "shouldForceConnectPhoneNumber")]
    should_force_connect_phone_number: bool,

    #[serde(rename = "picture")]
    picture: String,

    #[serde(rename = "learningLanguage")]
    learning_language: String,

    #[serde(rename = "hasFacebookId")]
    has_facebook_id: bool,

    #[serde(rename = "shakeToReportEnabled")]
    shake_to_report_enabled: Option<serde_json::Value>,

    #[serde(rename = "liveOpsFeatures")]
    live_ops_features: Vec<Option<serde_json::Value>>,

    #[serde(rename = "canUseModerationTools")]
    can_use_moderation_tools: bool,

    #[serde(rename = "id")]
    id: i64,

    #[serde(rename = "betaStatus")]
    beta_status: String,

    #[serde(rename = "hasGoogleId")]
    has_google_id: bool,

    #[serde(rename = "privacySettings")]
    privacy_settings: Vec<Option<serde_json::Value>>,

    #[serde(rename = "fromLanguage")]
    from_language: String,

    #[serde(rename = "hasRecentActivity15")]
    has_recent_activity15: bool,

    #[serde(rename = "_achievements")]
    achievements: Vec<Option<serde_json::Value>>,

    #[serde(rename = "observedClassroomIds")]
    observed_classroom_ids: Vec<Option<serde_json::Value>>,

    #[serde(rename = "username")]
    username: String,

    #[serde(rename = "bio")]
    bio: String,

    #[serde(rename = "profileCountry")]
    profile_country: Option<String>,

    #[serde(rename = "globalAmbassadorStatus")]
    global_ambassador_status: GlobalAmbassadorStatus,

    #[serde(rename = "currentCourseId")]
    current_course_id: String,

    #[serde(rename = "hasPhoneNumber")]
    has_phone_number: bool,

    #[serde(rename = "creationDate")]
    creation_date: i64,

    #[serde(rename = "achievements")]
    user_achievements: Vec<Option<serde_json::Value>>,

    #[serde(rename = "hasPlus")]
    has_plus: bool,

    #[serde(rename = "name")]
    #[serde(default)]
    name: Option<String>,

    #[serde(rename = "roles")]
    roles: Vec<String>,

    #[serde(rename = "classroomLeaderboardsEnabled")]
    classroom_leaderboards_enabled: bool,

    #[serde(rename = "emailVerified")]
    email_verified: bool,

    #[serde(rename = "courses")]
    courses: Vec<Course>,

    #[serde(rename = "totalXp")]
    total_xp: i64,

    #[serde(rename = "streakData")]
    streak_data: StreakData,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Course {
    #[serde(rename = "preload")]
    preload: bool,

    #[serde(rename = "placementTestAvailable")]
    placement_test_available: bool,

    #[serde(rename = "authorId")]
    author_id: String,

    #[serde(rename = "title")]
    title: String,

    #[serde(rename = "learningLanguage")]
    learning_language: String,

    #[serde(rename = "xp")]
    xp: i64,

    #[serde(rename = "healthEnabled")]
    health_enabled: bool,

    #[serde(rename = "fromLanguage")]
    from_language: String,

    #[serde(rename = "crowns")]
    crowns: i64,

    #[serde(rename = "id")]
    id: String,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct GlobalAmbassadorStatus {
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct StreakData {
    #[serde(rename = "currentStreak")]
    current_streak: Option<serde_json::Value>,
}

pub fn get_by_username(username: String) -> Result<reqwest::blocking::Response, reqwest::Error> {
    let client = reqwest::blocking::Client::new();
    let resp = client.get(&format!("{}", &ENDPOINT_USERNAME.replace("{{USERNAME}}", &username)))
    .timeout(Duration::from_secs(30))
    .header("User-Agent", "Mozilla/5.0 (Windows NT 10.0; rv:110.0) Gecko/20100101 Firefox/110.0")
    .send()?;

    match resp.status() {
        reqwest::StatusCode::OK => {
            Ok(resp)
        },
        reqwest::StatusCode::NOT_FOUND => {
            println!("User not found");
            exit(0)
        },
        _ => { 
            panic!("Something happened"); 
        }
    }
}

fn main() {
    let args: Vec<String> = std::env::args().collect();
    
    match args.len() {
        1 => {
            println!("USAGE: ./duolingoOSINT <username>");
        },
        2 => {
            let res = get_by_username(args[1].parse().unwrap()).unwrap();
            let data = &res.json::<Duolingo>().unwrap();

            if data.users.len() == 0 {
                println!("user not found");
                exit(0)
            }

            let user = &data.users[0];

            println!("Username {}", user.username);

            if user.name.is_some() {
                println!("Name: {}", user.name.as_ref().map(String::as_str).unwrap());
            }
            println!("Avatar: https:{}/xxlarge", user.picture);
            println!("Streak: {}", user.streak);
            println!("Motivation: {}", user.motivation);
            println!("Plus: {}", user.has_plus);
            
            if !user.profile_country.is_none() {
                println!("Country: {}", rust_iso3166::from_alpha2(&user.profile_country.as_ref().map(String::as_str).unwrap()).unwrap().name);
            }
            
            println!("Created At: {:#?}", Local.timestamp_opt(user.creation_date, 0).unwrap().to_string());
            println!("Courses: ({})", user.courses.len());
            
            for val in user.courses.iter() {
                println!("\t{} -> {} [{}]\n\t\tXP: {}\n\t\tCrowns: {}", 
                isolang::Language::from_639_1(&val.from_language).unwrap().to_name(), 
                isolang::Language::from_639_1(&val.learning_language).unwrap().to_name(), 
                val.id,
                val.xp,
                val.crowns);
            }
        }
        _ => panic!("USAGE: ./duolingoOSINT <username>")
    }
}
